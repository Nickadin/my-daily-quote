defmodule DailyQuotes.QuoteView do
  use DailyQuotes.Web, :view

  def render("random.json", %{data: data}) do
    data
  end
end
