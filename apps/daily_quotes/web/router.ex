defmodule DailyQuotes.Router do
  use DailyQuotes.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DailyQuotes do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", DailyQuotes do
    pipe_through :api

    scope "/quotes" do
      get "/random", QuoteController, :random
    end
  end
end
