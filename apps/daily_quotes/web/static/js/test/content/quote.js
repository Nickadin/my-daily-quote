import {describe, it} from 'mocha'

import React from 'react'
import chai from 'chai'
import sinon from 'sinon'

import chaiEnzyme from 'chai-enzyme'
import sinonChai from 'sinon-chai'

chai.should()
chai.use(chaiEnzyme())
chai.use(sinonChai)

const expect = chai.expect

import Quote from '../../content/quote'
import Promise from 'bluebird'

import {shallow} from 'enzyme'

describe('<Quote />', () => {
  it('should load the quote', () => {
    const quote = {id: 1, quote: "foo"}
    const stub = sinon.stub().returns(Promise.resolve(quote))

    const wrapper = shallow(<Quote
      location={{pathname: "foo"}}
      getRandomQuote={stub}
    />)
    expect(stub).to.be.called;
  })
})