//We dont use redux but we keep the structure a bit similar, at least split up the requests

import axios from 'axios'
const URL = 'http://localhost:4000/api/quotes'

export const getRandomQuote = ({category}) => {
  const url = `${URL}/random?category=${category}`

  return axios.get(url).then(response => {
    return response.data
  })
}