import React, {Component} from 'react'
import { RouteTransition } from 'react-router-transition'

import QuoteText from './quote_text'

import Dropdown from './dropdown'

const CATEGORIES = ['programmer', 'inspirational']

export default class Quote extends Component {
  constructor(props) {
    super(props)

    this.getQuote = this.getQuote.bind(this)
    this.onSelect = this.onSelect.bind(this)
    this.state = {quote: null, showDropdown: false, category: 'programmer'}
  }

  componentWillMount() {
    this.getQuote()
  }

  getQuote() {
    const {getRandomQuote} = this.props
    const {category} = this.state

    return getRandomQuote({category}).then(({quote}) => this.setState({quote}))
  }

  toggleCategory() {
    this.setState({showDropdown: !this.state.showDropdown})
  }

  onSelect(category) {
    const {getRandomQuote} = this.props
    return getRandomQuote({category}).then(({quote}) => this.setState({quote, category, showDropdown: false}))
  }

  render() {
    const {quote} = this.state

    if (!quote) {
      return <div>"Loading..."</div>
    }

    const classes = this.state.showDropdown ? "category-dropdown" : "hide"

    return (
      <RouteTransition
        pathname={this.props.location.pathname}
        atEnter={{ opacity: 0 }}
        atLeave={{ opacity: 0 }}
        atActive={{ opacity: 1 }}
        component="div"
        className="quote-container"
      >
        <QuoteText key={quote} quote={quote} />
        <div className="btn-row">
          <a href="#" className="btn btn-full divide-right" onClick={() => this.getQuote()}>Give me a new quote!</a>
          <a href="#" className="btn" onClick={() => this.toggleCategory()}>Change category</a>
          <div className={classes}>
            <Dropdown options={CATEGORIES} onSelect={this.onSelect} />
          </div>
        </div>
        <span className="small-notice">Current category: {this.state.category}</span>

      </RouteTransition>
    )
  }
}