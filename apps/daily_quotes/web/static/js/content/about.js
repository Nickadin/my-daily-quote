import React from 'react'
import { RouteTransition } from 'react-router-transition'

export default (props) => {
  return (
    <RouteTransition
      pathname={props.location.pathname}
      atEnter={{ opacity: 0 }}
      atLeave={{ opacity: 0 }}
      atActive={{ opacity: 1 }}
    >
      <h3>About this page</h3>
      <p>
        This app is meant for users to get a random, inspirational quote.<br />
        It is part of an assignment assigned to me, to show my Frontend and Backend capabilities.<br />
        <br />
        Please enjoy using this app!
      </p>
    </RouteTransition>
  )
}