import React from 'react'

import Quote from './quote'
import About from './about'

import {getRandomQuote} from '../actions/quotes'
import { RouteTransition } from 'react-router-transition'
import {Route} from 'react-router-dom'

export default (props) => {
  return (
    <div className="main-content">
      <Route exact path="/" render={(props) => <Quote getRandomQuote={getRandomQuote} {...props} />} />
      <Route path="/about" component={About} />
    </div>
  )
}