import React from 'react'

import _ from 'lodash'

export default ({options, onSelect}) => {
  return (
    <ul className="dropdown-menu">
      {_.map(options, option => {
        return <li
          key={option}
          onClick={() => onSelect(option)}
        >{option}</li>
      })}
    </ul>
  )
}