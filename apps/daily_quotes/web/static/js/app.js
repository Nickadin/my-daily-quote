import React from 'react'
import ReactDOM from 'react-dom'

import Background from './layout/background'

import Logo from './content/logo'
import Navigation from './content/navigation'
import MainContent from './content/main'
import About from './content/about'

import CSSTransition from 'react-addons-css-transition-group'
//http://quotes.stormconsultancy.co.uk/quotes/random.json

import {
  HashRouter as Router, //For the sake of this exercise, hashrouter will do
} from 'react-router-dom'

ReactDOM.render(
  <Router>
    <div className="container">
      <Background />
      <Logo />
      <Navigation />
      <MainContent />
    </div>
  </Router>
, document.querySelector('#root'))
