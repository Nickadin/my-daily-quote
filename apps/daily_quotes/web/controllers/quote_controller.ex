defmodule DailyQuotes.QuoteController do
  use DailyQuotes.Web, :controller

  @quote_repo Application.get_env(:daily_quotes, :quote_repo)

  def random(conn, payload) do
    case @quote_repo.random(payload) do
      {:ok, quote} ->
        render(conn, %{data: quote})
      _ ->
        conn
        |> put_status(500)
        |> render(%{data: %{message: "Something went wrong"}})
    end
  end
end
