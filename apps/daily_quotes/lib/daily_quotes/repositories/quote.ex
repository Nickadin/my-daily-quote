defmodule DailyQuotes.Repositories.Quote do
  def random(payload) do
    atom_keyed_payload = for {k, v} <- payload, do: {String.to_atom(k), v}, into: %{}
    Quote.Services.Quote.get(atom_keyed_payload)
  end
end