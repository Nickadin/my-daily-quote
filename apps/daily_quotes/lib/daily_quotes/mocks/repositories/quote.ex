defmodule DailyQuotes.Mocks.Repositories.Quote do
  def random(_) do
    {:ok, %{id: 1, quote: "My test quote"}}
  end
end