use Mix.Config

config :daily_quotes,
  quote_repo: DailyQuotes.Mocks.Repositories.Quote

config :daily_quotes, DailyQuotes.Endpoint,
  http: [port: 4001],
  server: false

config :logger, level: :warn
