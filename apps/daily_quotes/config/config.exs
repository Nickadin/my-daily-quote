use Mix.Config

config :daily_quotes,
  quote_repo: DailyQuotes.Repositories.Quote

config :daily_quotes, DailyQuotes.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "73D9BywaAskwJDowEvuY3k4ksGZyptsguBE/qwR6Gv0HQdpq1w7HUQPRj6xTFP2g",
  render_errors: [view: DailyQuotes.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DailyQuotes.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

import_config "#{Mix.env}.exs"
