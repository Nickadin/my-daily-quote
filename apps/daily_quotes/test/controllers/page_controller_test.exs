defmodule DailyQuotes.QuoteControllerTest do
  use DailyQuotes.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "My Daily Quote"
  end
end
