defmodule DailyQuotes.PageControllerTest do
  use DailyQuotes.ConnCase

  test "GET /api/quotes/random", %{conn: conn} do
    conn = get conn, "/api/quotes/random?category=programmer"
    assert json_response(conn, 200) === %{"id" => 1, "quote" => "My test quote"}
  end
end
