defmodule Quote do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      supervisor(Quote.Supervisors.QuoteSupervisor, []),
      supervisor(Quote.Repo, [])
    ]

    opts = [strategy: :one_for_one, name: Exercises.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
