defmodule Quote.Repositories.Storm do
  import Quote.Models.Quote
  @base_url "http://quotes.stormconsultancy.co.uk"

  def random do
    response = HTTPoison.get "#{@base_url}/random.json"
    case response do
      {:ok, %HTTPoison.Response{status_code: 200} = http_response } ->
        %HTTPoison.Response{body: body} = http_response
        quote = body
        |> Poison.decode!
        |> to_model()

        {:ok, quote}
      _ -> :error #For the sake of this small application, this will do
    end
  end
end