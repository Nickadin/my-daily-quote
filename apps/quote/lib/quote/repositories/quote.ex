defmodule Quote.Repositories.Quote do
  def random do
    quote = Quote.Repo.all(Quote.Schemas.Quote)
    |> Enum.random
    |> Map.from_struct
    |> Quote.Models.Quote.to_model

    {:ok, quote}
  end
end