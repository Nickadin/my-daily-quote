defmodule Quote.Models.Quote do
  @doc """
  Converts a quote to the specified API model

  Example

  iex> Quote.to_model(%{"id" => 1, "quote" => "foo", "author": "nick", "permalink": "no way"})
  %{id: 1, quote: "foo"}
  """
  def to_model(%{"id" => id, "quote" => quote}), do: %{id: id, quote: quote}

  def to_model(%{id: id, quote: quote}), do: %{id: id, quote: quote}
end