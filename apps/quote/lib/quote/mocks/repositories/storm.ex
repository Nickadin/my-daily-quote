defmodule Quote.Mocks.Repositories.Storm do
  @stub_quote %{
    id: 1,
    quote: "My test quote"
  }

  def random do
    {:ok, @stub_quote}
  end
end