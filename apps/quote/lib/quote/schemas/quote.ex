defmodule Quote.Schemas.Quote do
  use Ecto.Schema

  schema "quotes" do
    field :quote, :string
  end
end