defmodule Quote.Services.Quote do
  use GenServer

  @storm_quote_repo Application.get_env(:quote, :storm_repo)

  def start_link(_args) do
    GenServer.start_link(__MODULE__, [])
  end

  def init(_) do
    {:ok, []}
  end

  @doc """
  Gets a random quote from the source http://quotes.stormconsultancy.co.uk/random.json or
  the inspirational quotes from our data, depending on the category.

  Category should be either "programmer" or "inspirational"

  Example
  iex> Quote.get %{category: "programmer"}
  {:ok, %{id: 1, quote: "My test quote"}}
  """

  def get(payload \\ %{}) do
    :poolboy.transaction(:quote_pool, fn pid ->
      GenServer.call(pid, {:get, payload})
    end)
  end

  def handle_call({:get, %{category: "programmer"}}, _from, state) do
    result = case @storm_quote_repo.random do
      {:ok, quote} -> {:ok, quote}
      _ -> :error
    end

    {:reply, result, state}
  end

  def handle_call({:get, %{category: "inspirational"}}, _from, state) do
    result = case Quote.Repositories.Quote.random do
      {:ok, quote} -> {:ok, quote}
      _ -> :error
    end
    {:reply, result, state}
  end

  def handle_call({:get, _}, _from, state) do
    error = {:error, {:badrequest, "The request was deemed invalid", [category: :required]}}
    {:reply, error, state}
  end
end