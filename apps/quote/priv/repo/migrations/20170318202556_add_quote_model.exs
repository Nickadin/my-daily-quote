defmodule Quote.Repo.Migrations.AddQuoteModel do
  use Ecto.Migration

  def change do
    create table(:quotes) do
      add :quote, :string, null: false
    end
  end
end
