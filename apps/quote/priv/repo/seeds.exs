[
  %{
    id: 1,
    quote: "You can do anything, but not everything.",
    author: "David Allen"
  },
  %{
    id: 2,
    quote: "Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away",
    author: "Antoine de Saint-Exupéry"
  },
  %{
    id: 3,
    quote: "The richest man is not he who has the most, but he who needs the least.",
    author: "Unknown Author"
  },
  %{
    id: 4,
    quote: "You miss 100 percent of the shots you never take.",
    author: "Wayne Gretzky"
  },
  %{
    id: 5,
    quote: "You must be the change you wish to see in the world.",
    author: "Gandhi"
  }
]
|> Enum.map(fn %{quote: quote} ->
  Quote.Repo.insert!(%Quote.Schemas.Quote{quote: quote})
end)