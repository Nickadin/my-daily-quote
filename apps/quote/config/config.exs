use Mix.Config

config :quote,
  ecto_repos: [Quote.Repo]

config :quote, Quote.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "quotes",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"


config :quote, storm_repo: Quote.Repositories.Storm

import_config "#{Mix.env}.exs"
