use Mix.Config

config :quote, storm_repo: Quote.Mocks.Repositories.Storm

config :quote, Quote.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "quotes_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"