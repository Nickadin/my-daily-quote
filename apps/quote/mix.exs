defmodule Quote.Mixfile do
  use Mix.Project

  def project do
    [app: :quote,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger],
     mod: {Quote, [
       :poolboy, :httpoison, :poison, :ecto, :postgres
     ]}]
  end

  defp deps do
    [
      {:poolboy, "~> 1.5"},
      {:httpoison, "~> 0.10.0"},
      {:poison, "~> 2.0"},
      {:ecto, "~> 2.1"},
      {:postgrex, ">= 0.0.0"}
    ]
  end
end
