defmodule Quote.Services.QuoteTest do
  use ExUnit.Case

  alias Quote.Services

  doctest Quote

  describe "#get" do
    test "should return :badrequest if no category is given" do
      expected = {:error, {:badrequest, "The request was deemed invalid", [category: :required]}}

      assert expected === Services.Quote.get
    end

    test "should return a random quote of programmer category if category programmer is given" do
      expected = %{quote: "My test quote", id: 1}

      assert {:ok, expected} === Services.Quote.get(%{category: "programmer"})
    end

    test "should return a random quote of inspirational category if category inspirational is given" do
      %{quote: quote} = Quote.Repo.insert!(%Quote.Schemas.Quote{quote: "My quote"})

      assert {:ok, %{quote: ^quote}} = Services.Quote.get(%{category: "inspirational"})
    end
  end
end