# MyDailyQuote

This application is part of an assigned that I've been request to make. It will fetch a random inspirational quote for the user.

## Technology Stack

The following technology stack is used.

### React.JS

I've chosen to use pure React.JS without any additional libraries such as redux. The size of this app does not warrant the use of a framework such as redux, and it's easy enough to contain the small amount of state we need.

### Elixir + Phoenix

Elixir has been chosen for the backend side. While elixir definitely is not required for a project this small, and many of its capabilities are not used, I like using elixir because of the language itself. It also provides access to umbrella projects, which I've taken a particular liking to.

## Running the app

### Prerequisites

1. Have erlang + elixir + phoenix installed
2. Have node.js / npm installed
3. Run ```npm install``` in the apps/my-daily-quote directory

### Starting the app

To run the app, use:
```iex -S mix phoenix.server```*

\* Obviously this will be different in full fledge production scenarios.


## Running the tests

To run the tests, use:

```mix test```